package cz.upce.chat.application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class Client {

    private static final Logger logger = Logger.getLogger(Client.class.getName());
    
    private static final String SERVER = "localhost";
    private static final int PORT = 8080;
    private static final String END_TAG = "</Message>";
    
    private final String endLine;
    
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private boolean serverAlive = true;
    
    public Client() {
        this.endLine = System.lineSeparator();
        
        try {
            this.socket = new Socket(SERVER, PORT);
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            
            sendRequest(REQUEST_TYPE.LOGIN, "empty");
            
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }
    
    public void listenToServer() {
        try {
            while(serverAlive) {
                String requestString = reader.readLine();
            }
            
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }
    
    private void sendRequest(REQUEST_TYPE request, String content) {
        
        try {
            writer.append(request.toString() + endLine);
            writer.append(content.trim() + endLine);
            writer.append(END_TAG + endLine);
            writer.flush();
            
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }
    
    
}
