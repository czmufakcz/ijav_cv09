package cz.upce.chat.application;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

import cz.upce.chat.application.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class FXMLMainController implements Initializable, Observer {

    private Client client;

    @FXML
    private ListView<String> onlineUsers;

    @FXML
    private Button loginBtn;
    
    @FXML
    private TextField loginText;

    public FXMLMainController(Client client) {
        this.client = client;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.client.addObserver(this);
        loginBtn.setOnAction((ActionEvent event) -> {
            this.client.login(loginText.getText());
        });

    }

    @Override
    public void update(Observable observable, Object arg) {
        if(observable instanceof Client) {
            ObservableList<String> users = FXCollections.observableList(client.getUsersOnline());
            onlineUsers.setItems(users);
        }
        
    }

}
