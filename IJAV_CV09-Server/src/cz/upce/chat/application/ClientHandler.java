package cz.upce.chat.application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Logger;

public class ClientHandler extends Thread {

    private static final Logger logger = Logger.getLogger(ClientHandler.class.getName());
    private static final String END_TAG = "</Message>";

    private final String endLine;
    private final Socket socket;

    private BufferedReader reader;
    private BufferedWriter writer;
    private boolean clientAlive = true;

    public ClientHandler(Socket socket) {
        this.endLine = System.lineSeparator();
        this.socket = socket;
        try {
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }

    @Override
    public void run() {
        super.run();

        try {
            while (clientAlive) {
                String requestString = reader.readLine();
                REQUEST_TYPE request = REQUEST_TYPE.valueOf(requestString);
                
                switch (request) {
                case LOGIN:
                    processLoginRequest(readStringFromReader());
                    break;
                default:
                    logger.warning("Unknown message.");
                    break;
                }

            }
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }

    }
    
    private void processLoginRequest(String content) {
        logger.info("LOGIN INFO");
        logger.info(content);
    }

    private String readStringFromReader() {
        StringBuilder builder = new StringBuilder();
        String breakString;
        try {
            while (!(breakString = reader.readLine()).contains(END_TAG)) {
                builder.append(breakString)
                       .append(endLine);
            }
            
            builder.delete(builder.lastIndexOf(endLine), builder.length());
            return builder.toString();
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
        return "";
    }

}
