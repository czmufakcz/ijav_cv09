package cz.upce.chat.application;

public enum REQUEST_TYPE {
    LOGIN, 
    LOGOUT, 
    ONLINE_USER,
    OFFLINE_USER,
    ALL_USERS,
    CLOSE_CONNECTION
}
