package cz.upce.chat.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class Server {

    private static final Logger logger = Logger.getLogger(Server.class.getName());
    private static final int PORT = 8080;
    
    private ServerSocket serverSocket;
    
    public Server() {
        try {
            this.serverSocket = new ServerSocket(PORT);
            logger.info("Server started.");
            
            while(true) {
                Socket socket = this.serverSocket.accept();
                logger.info("Client connected");
                
                ClientHandler clientHandler = new ClientHandler(socket);
                clientHandler.start();
            }
            
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }
}
