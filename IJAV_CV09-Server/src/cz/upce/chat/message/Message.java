package cz.upce.chat.message;
import java.time.LocalDateTime;

import cz.upce.chat.user.User;

public class Message {

    private int id;
    private User own;
    private User toUser;
    private String message;
    private LocalDateTime created;
    
    public Message(User own, User toUser, String message) {
        super();
        this.own = own;
        this.toUser = toUser;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getOwn() {
        return own;
    }

    public void setOwn(User own) {
        this.own = own;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Message [id=" + id + ", own=" + own + ", toUser=" + toUser + ", message=" + message + ", created=" + created + "]";
    }

}
