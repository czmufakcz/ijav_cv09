package cz.upce.chat.message;

import java.time.LocalDateTime;
import java.util.Collection;

public class MessageModel {
    private final IMessageRepository messageRepository;

    public MessageModel() {
        this.messageRepository = new MessageRepository();
    }

    public Message save(Message message) {
        message.setCreated(LocalDateTime.now());
        messageRepository.save(message);
        return message;
    }

    public Message removeById(int id) {
        return messageRepository.removeById(id);
    }

    public Collection<Message> getMessages(int page, int count) {
        return messageRepository.getMessages(page, count);
    }

}
