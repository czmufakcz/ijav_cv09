package cz.upce.chat.message;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class MessageRepository implements IMessageRepository {

    private static int ID_SEQ = 0;
    private final List<Message> messages = new LinkedList<Message>();

    @Override
    public synchronized Message save(Message message) {
        message.setId(ID_SEQ++);
        messages.add(message);
        return message;
    }

    @Override
    public synchronized Message removeById(int id) {
        Message removedMessage = messages.stream()
                                         .filter(message -> id == message.getId())
                                         .findFirst()
                                         .get();
        return removedMessage;
    }

    @Override
    public synchronized Collection<Message> getMessages(int page, int count) {
        return messages.stream()
                       .skip(count * (page - 1))
                       .limit(count)
                       .collect(Collectors.toList());
    }

}
