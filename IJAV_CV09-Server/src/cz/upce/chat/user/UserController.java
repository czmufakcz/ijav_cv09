package cz.upce.chat.user;

import java.util.Collection;
import java.util.logging.Logger;

public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class.getName());
    private static final int MIN_AGE = 18;

    private final UserModel userModel;

    public UserController() {
        this.userModel = new UserModel();
    }

    public User save(User user) {
        if (user == null) {
            String message = "User is null";
            LOGGER.warning(message);
            throw new NullPointerException(message);
        }
        
        this.validateName(user.getUsername());
        
        if(userModel.isExistUsername(user.getUsername())) {
            String message = "Username is exist.";
            LOGGER.warning(message);
            throw new IllegalArgumentException(message);
        }

        if (user.getPassword() == null || user.getPassword() == "") {
            String message = "Incorrect password";
            LOGGER.warning(message);
            throw new NullPointerException(message);
        }

        if (user.getAge() < MIN_AGE) {
            String message = "User is younger " + MIN_AGE + " years.";
            LOGGER.warning(message);
            throw new IllegalArgumentException(message);
        }
        User userSaved = userModel.save(user);
        LOGGER.info("User cretead: " + user);
        return userSaved;
    }

    public User findByUsername(String name) {
        this.validateName(name);
        return userModel.findByUsername(name);
    }

    public User removeByUsername(String name) {
        this.validateName(name);
        return userModel.removeByUsername(name);
    }

    public Collection<User> getAllUsers() {
        return userModel.getAllUsers();
    }

    private void validateName(String name) {
        if (name == null || name == "") {
            String message = "Name is null or empty.";
            LOGGER.warning(message);
            throw new NullPointerException(message);
        }
    }
}
