package cz.upce.chat.user;
import java.time.LocalDateTime;
import java.util.Collection;

public class UserModel {

    private final IUserRepository userRepository;
    
    public UserModel() {
        this.userRepository = new UserRepository();
    }
    
    public User save(User user) {
        user.setCreated(LocalDateTime.now());
        return userRepository.save(user);
    }
    
    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }
    
    public User removeByUsername(String name) {
        return userRepository.removeByUsername(name);
    }

    public boolean isExistUsername(String name) {
        return userRepository.isExistUsername(name);
    }
    
    public Collection<User> getAllUsers(){
        return userRepository.getAllUsers();
    }
    
}
