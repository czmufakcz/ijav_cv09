# Zadání úkolů 
1) Komunikace Server-Client
    - První řádek bude obsahovat vždy typ požadavku
        - LOGIN - přihlášení uživatele
        - LOGOUT - odhlášení uživate
        - ONLINE_USER - uživatel, který zrovna přihlásil
        - OFFLINE_USER - uživatel, který se zrovna odhlásil
        - ALL_USERS - seznam všech přihlášených uživatelů
        - CLOSE_CONNECTION - uzavření spojení mezi klientem a serverem
    - druhý řádek a více obsahuje zprávy
2) Rozšiřte CV07 jako stranu serveru
3) Vytvořte nový projekt jako klienta a implementujte